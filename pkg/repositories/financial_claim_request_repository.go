// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/model"
)

type FinancialClaimRequestRepository interface {
	List() (*[]model.Configuration, error)
	Create(configuration model.Configuration) (*model.Configuration, error)
	Get(id string) (*model.Configuration, error)
	Update(id string, configuration model.Configuration) (*model.Configuration, error)
	Delete(id string) (*model.Configuration, error)
	healthcheck.Checker
}
