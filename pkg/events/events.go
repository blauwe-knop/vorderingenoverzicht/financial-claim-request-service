package events

var (
	FCRS_1 = NewEvent(
		"fcrs_1",
		"started listening",
		Low,
	)
	FCRS_2 = NewEvent(
		"fcrs_2",
		"server closed",
		High,
	)
	FCRS_3 = NewEvent(
		"fcrs_3",
		"received request for openapi spec as JSON",
		Low,
	)
	FCRS_4 = NewEvent(
		"fcrs_4",
		"sent response with openapi spec as JSON",
		Low,
	)
	FCRS_5 = NewEvent(
		"fcrs_5",
		"received request for openapi spec as YAML",
		Low,
	)
	FCRS_6 = NewEvent(
		"fcrs_6",
		"sent response with openapi spec as YAML",
		Low,
	)
	FCRS_8 = NewEvent(
		"fcrs_8",
		"failed to read openapi.json file",
		High,
	)
	FCRS_9 = NewEvent(
		"fcrs_9",
		"failed to write fileBytes to response",
		High,
	)
	FCRS_10 = NewEvent(
		"fcrs_10",
		"failed to read openapi.yaml file",
		High,
	)
	FCRS_11 = NewEvent(
		"fcrs_11",
		"failed to list configuration",
		High,
	)
	FCRS_12 = NewEvent(
		"fcrs_12",
		"received request to list configurations",
		Low,
	)
	FCRS_13 = NewEvent(
		"fcrs_13",
		"failed to encode response payload",
		High,
	)
	FCRS_14 = NewEvent(
		"fcrs_14",
		"sent response with configuration list",
		Low,
	)
	FCRS_15 = NewEvent(
		"fcrs_15",
		"received request to create configuration",
		Low,
	)
	FCRS_16 = NewEvent(
		"fcrs_16",
		"failed to decode payload",
		High,
	)
	FCRS_17 = NewEvent(
		"fcrs_17",
		"error creating configuration",
		High,
	)
	FCRS_18 = NewEvent(
		"fcrs_18",
		"failed to encode response payload",
		High,
	)
	FCRS_19 = NewEvent(
		"fcrs_19",
		"sent response with created configuration",
		Low,
	)
	FCRS_20 = NewEvent(
		"fcrs_20",
		"received request to get configuration",
		Low,
	)
	FCRS_21 = NewEvent(
		"fcrs_21",
		"configuration not found",
		Low,
	)
	FCRS_22 = NewEvent(
		"fcrs_22",
		"error retrieving configuration",
		High,
	)
	FCRS_23 = NewEvent(
		"fcrs_23",
		"failed to encode response payload",
		High,
	)
	FCRS_24 = NewEvent(
		"fcrs_24",
		"sent response with encoded configuration",
		Low,
	)
	FCRS_25 = NewEvent(
		"fcrs_25",
		"received request to update handler",
		Low,
	)
	FCRS_26 = NewEvent(
		"fcrs_26",
		"failed to decode request payload",
		High,
	)
	FCRS_27 = NewEvent(
		"fcrs_27",
		"failed to get configuration",
		High,
	)
	FCRS_28 = NewEvent(
		"fcrs_28",
		"failed to update configuration",
		High,
	)
	FCRS_29 = NewEvent(
		"fcrs_29",
		"failed to encode response payload",
		High,
	)
	FCRS_30 = NewEvent(
		"fcrs_30",
		"send encoded updated configuration",
		Low,
	)
	FCRS_31 = NewEvent(
		"fcrs_31",
		"received delete request",
		Low,
	)
	FCRS_32 = NewEvent(
		"fcrs_32",
		"failed to get configuration",
		High,
	)
	FCRS_33 = NewEvent(
		"fcrs_33",
		"error deleting configuration",
		High,
	)
	FCRS_34 = NewEvent(
		"fcrs_34",
		"failed to encode response payload",
		High,
	)
	FCRS_35 = NewEvent(
		"fcrs_35",
		"send encoded deleted configuration",
		Low,
	)
	FCRS_36 = NewEvent(
		"fcrs_36",
		"error creating configuration repository",
		High,
	)
)
