package model

type Configuration struct {
	Token        string   `json:"token"`
	Bsn          string   `json:"bsn"`
	CreatedAt    JSONTime `json:"createdAt"`
	ExpiresAt    JSONTime `json:"expiresAt"`
	AppPublicKey string   `json:"appPublicKey"`
}
