// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import "fmt"

type FinancialClaimRequestDB struct {
	Host     string `long:"financial-claim-request-db-host" env:"FINANCIAL_CLAIM_REQUEST_DB_HOST" default:"financial-claim-request-db-rw" description:"FinancialClaimRequestDB host"`
	Port     uint16 `long:"financial-claim-request-db-port" env:"FINANCIAL_CLAIM_REQUEST_DB_PORT" default:"5432" description:"FinancialClaimRequestDB port"`
	Username string `long:"financial-claim-request-db-username" env:"FINANCIAL_CLAIM_REQUEST_DB_USERNAME" description:"FinancialClaimRequestDB username"`
	Password string `long:"financial-claim-request-db-password" env:"FINANCIAL_CLAIM_REQUEST_DB_PASSWORD" description:"FinancialClaimRequestDB password"`
	Database string `long:"financial-claim-request-db-database" env:"FINANCIAL_CLAIM_REQUEST_DB_DATABASE" default:"financial-claim-request-db" description:"FinancialClaimRequestDB database name"`
	SSLMode  string `long:"financial-claim-request-db-sslmode" env:"FINANCIAL_CLAIM_REQUEST_DB_SSLMODE" default:"require" description:"FinancialClaimRequestDB sslmode"`

	MaxIdleConns_ int `long:"financial-claim-request-db-max-idle-conns" env:"FINANCIAL_CLAIM_REQUEST_DB_MAX_IDLE_CONNS" default:"5" description:"Maximum idle connections allowed in the pg db pool."`
	MaxOpenConns_ int `long:"financial-claim-request-db-max-open-conns" env:"FINANCIAL_CLAIM_REQUEST_DB_MAX_OPEN_CONNS" default:"10" description:"Maximum open connections allowed in the pg db pool."`
}

// DSN returns a DSN string for the FinancialClaimRequestDB parameters
func (d FinancialClaimRequestDB) DSN() string {
	return fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s", d.Host, d.Port, d.Database, d.Username, d.Password, d.SSLMode)
}

// DSNSafe returns a DSN string that is safe to print
func (d FinancialClaimRequestDB) DSNSafe() string {
	dCensored := d
	dCensored.Password = "CENSORED"
	return dCensored.DSN()
}

func (d FinancialClaimRequestDB) MaxIdleConns() int {
	return d.MaxIdleConns_
}

func (d FinancialClaimRequestDB) MaxOpenConns() int {
	return d.MaxOpenConns_
}
