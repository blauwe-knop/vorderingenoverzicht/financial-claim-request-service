// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"github.com/huandu/xstrings"
	_ "github.com/jackc/pgx/v4/stdlib" // imported to register pgx db/sql.Driver.
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type DBConnectionDetailer interface {
	DSN() string
	DSNSafe() string
	MaxIdleConns() int
	MaxOpenConns() int
}

func SetupDB(log *zap.Logger, detailer DBConnectionDetailer) (*sqlx.DB, error) {
	log.Debug("setting up postgres connection to db", zap.String("dsn", detailer.DSNSafe()))
	db, err := sqlx.Open("pgx", detailer.DSN())
	if err != nil {
		return nil, errors.Wrap(err, "failed opening postgres connection")
	}
	db.MapperFunc(xstrings.ToSnakeCase)
	db.SetMaxIdleConns(detailer.MaxIdleConns())
	db.SetMaxOpenConns(detailer.MaxOpenConns())
	err = db.Ping()
	if err != nil {
		return nil, errors.Wrap(err, "failed to ping database")
	}
	log.Debug("succesfully pinged db", zap.String("dsn", detailer.DSNSafe()))
	return db, nil
}

func SetupDBOrFatal(log *zap.Logger, detailer DBConnectionDetailer) *sqlx.DB {
	db, err := SetupDB(log, detailer)
	if err != nil {
		log.Fatal("failed setting up postgres connection", zap.Error(err), zap.String("dsn", detailer.DSNSafe()))
	}
	return db
}
