// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/events"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/internal/repositories"
)

type options struct {
	ListenAddress string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the financial-claim-request-service api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	APIKey        string `long:"api-key" env:"API_KEY" default:"" description:"Key to protect the API endpoints."`
	FinancialClaimRequestDB
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	logger.Debug("setting up postgres connection to financial claim request db", zap.String("dsn", cliOptions.FinancialClaimRequestDB.DSNSafe()))
	financialClaimRequestDB := SetupDBOrFatal(logger.With(zap.String("db", "financial-claim-request-service")), cliOptions.FinancialClaimRequestDB)

	configurationRepository, err := repositories.NewConfigurationDBClient(context.Background(), logger, financialClaimRequestDB)
	if err != nil {
		event := events.FCRS_36
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
	}

	router := http_infra.NewRouter(configurationRepository, cliOptions.APIKey, logger)
	event := events.FCRS_1
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAdress", cliOptions.ListenAddress), zap.Reflect("event", event))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.FCRS_2
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
