// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/events"

	"github.com/go-chi/chi/v5"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/model"
)

func handlerList(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.FCRS_12
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	configurationRepository, _ := context.Value(configurationRepositoryKey).(repositories.ConfigurationRepository)
	configuration, err := configurationRepository.List(context)
	if err != nil {
		event = events.FCRS_11
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*configuration)
	if err != nil {
		event = events.FCRS_13
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.FCRS_14
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerCreate(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()

	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.FCRS_15
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	configurationRepository, _ := context.Value(configurationRepositoryKey).(repositories.ConfigurationRepository)

	var configuration model.Configuration
	err := json.NewDecoder(request.Body).Decode(&configuration)
	if err != nil {
		event = events.FCRS_16
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	createdConfiguration, err := configurationRepository.Create(context, configuration)
	if err != nil {
		event = events.FCRS_17
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*createdConfiguration)
	if err != nil {
		event = events.FCRS_18
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.FCRS_19
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerGet(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.FCRS_20
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	configurationRepository, _ := context.Value(configurationRepositoryKey).(repositories.ConfigurationRepository)

	configuration, err := configurationRepository.Get(context, chi.URLParam(request, "id"))

	if err == repositories.ErrConfigurationNotFound {
		event = events.FCRS_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "Not Found", http.StatusNotFound)
		return
	}

	if err != nil {
		event = events.FCRS_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*configuration)
	if err != nil {
		event = events.FCRS_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.FCRS_24
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerUpdate(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	configurationRepository, _ := context.Value(configurationRepositoryKey).(repositories.ConfigurationRepository)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.FCRS_25
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var configuration model.Configuration
	err := json.NewDecoder(request.Body).Decode(&configuration)
	if err != nil {
		event = events.FCRS_26
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	id := chi.URLParam(request, "id")

	_, err = configurationRepository.Get(context, id)
	if err != nil {
		event = events.FCRS_27
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = configurationRepository.Update(context, configuration, id)
	if err != nil {
		event = events.FCRS_28
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	updatedConfiguration := &configuration

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*updatedConfiguration)
	if err != nil {
		event = events.FCRS_29
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.FCRS_30
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerDelete(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	configurationRepository, _ := context.Value(configurationRepositoryKey).(repositories.ConfigurationRepository)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.FCRS_31
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
	id := chi.URLParam(request, "id")

	configuration, err := configurationRepository.Get(context, id)
	if err != nil {
		event = events.FCRS_32
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = configurationRepository.Delete(context, id)
	if err != nil {
		event = events.FCRS_33
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	deletedConfiguration := configuration

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*deletedConfiguration)
	if err != nil {
		event = events.FCRS_34
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.FCRS_35
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
