// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/model"
)

type ConfigurationDBClient struct {
	log *zap.Logger

	stmtCreate      *sqlx.Stmt
	stmtList        *sqlx.Stmt
	stmtRead        *sqlx.Stmt
	stmtUpdate      *sqlx.Stmt
	stmtDelete      *sqlx.Stmt
	stmtHealthCheck *sqlx.Stmt
}

var ErrConfigurationNotFound = errors.New("organization does not exist")

func NewConfigurationDBClient(ctx context.Context, log *zap.Logger, financialClaimRequestDB *sqlx.DB) (*ConfigurationDBClient, error) {
	r := &ConfigurationDBClient{log: log}

	var err error

	r.stmtCreate, err = financialClaimRequestDB.PreparexContext(ctx, `
		INSERT INTO financial_claim_request_db.configurations (
			token,
			bsn,
			created_at,
			expires_at,
			app_public_key)
		VALUES (
			$1,
			$2,
			$3,
			$4,
			$5)
	`)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtCreate")
	}

	r.stmtList, err = financialClaimRequestDB.PreparexContext(ctx, `
		SELECT
			*
		FROM
			financial_claim_request_db.configurations
	`)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtRead")
	}

	r.stmtRead, err = financialClaimRequestDB.PreparexContext(ctx, `
		SELECT
			*
		FROM
			financial_claim_request_db.configurations
		WHERE
			token = $1
	`)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtRead")
	}

	r.stmtUpdate, err = financialClaimRequestDB.PreparexContext(ctx, `
		UPDATE
			financial_claim_request_db.configurations
		SET
			bsn = $2,
			created_at = $3,
			expires_at = $4,
			app_public_key = $5
		WHERE
			token = $1
	`)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtUpdate")
	}

	r.stmtDelete, err = financialClaimRequestDB.PreparexContext(ctx, `
		DELETE FROM financial_claim_request_db.configurations
		WHERE token = $1
	`)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtDelete")
	}

	r.stmtDelete, err = financialClaimRequestDB.PreparexContext(ctx, `
		DELETE FROM financial_claim_request_db.configurations
		WHERE token = $1
	`)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtDelete")
	}

	r.stmtHealthCheck, err = financialClaimRequestDB.PreparexContext(ctx, `
		SELECT 1
	`)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create stmtHealthCheck")
	}

	return r, nil
}

func (s *ConfigurationDBClient) Create(ctx context.Context, newConfiguration model.Configuration) (*model.Configuration, error) {
	s.log.Debug("Create", zap.String("newconfiguration.token", newConfiguration.Token))

	_, err := s.stmtCreate.ExecContext(ctx, newConfiguration.Token, newConfiguration.Bsn, newConfiguration.CreatedAt, newConfiguration.ExpiresAt, newConfiguration.AppPublicKey)

	if err != nil {
		return nil, errors.Wrap(err, "failed to create")
	}

	s.log.Debug("Created")

	return &newConfiguration, nil
}

func (s *ConfigurationDBClient) List(ctx context.Context) (*[]model.Configuration, error) {
	s.log.Debug("List")
	result := []model.Configuration{}

	err := s.stmtList.SelectContext(ctx, &result)

	if err != nil {
		return nil, errors.Wrap(err, "failed to list")
	}

	return &result, nil
}
func (s *ConfigurationDBClient) Get(ctx context.Context, id string) (*model.Configuration, error) {
	s.log.Debug("Get", zap.String("id", fmt.Sprint(id)))
	result := []model.Configuration{}

	err := s.stmtRead.SelectContext(ctx, &result, id)

	if err != nil {
		return nil, errors.Wrap(err, "failed to get")
	}

	if len(result) == 0 {
		return nil, ErrConfigurationNotFound
	}

	return &result[0], nil
}

func (s *ConfigurationDBClient) Update(ctx context.Context, newConfiguration model.Configuration, id string) error {
	s.log.Debug("Update", zap.String("id", fmt.Sprint(id)))

	_, err := s.stmtUpdate.ExecContext(ctx, id, newConfiguration.Bsn, newConfiguration.CreatedAt, newConfiguration.ExpiresAt, newConfiguration.AppPublicKey)

	if err != nil {
		return errors.Wrap(err, "failed to update")
	}

	return nil
}

func (s *ConfigurationDBClient) Delete(ctx context.Context, id string) error {
	s.log.Debug("Delete", zap.String("id", fmt.Sprint(id)))
	_, err := s.stmtDelete.ExecContext(ctx, id)

	if err != nil {
		return errors.Wrap(err, "failed to delete")
	}

	return nil
}

func (s *ConfigurationDBClient) GetHealthCheck() healthcheck.Result {
	name := "financial-claim-request-db"
	start := time.Now()

	_, err := s.stmtHealthCheck.Exec()

	if err != nil {
		log.Printf("failed to check health: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	return healthcheck.Result{
		Name:         name,
		Status:       healthcheck.StatusOK,
		ResponseTime: time.Since(start).Seconds(),
		HealthChecks: []healthcheck.Result{},
	}

}
