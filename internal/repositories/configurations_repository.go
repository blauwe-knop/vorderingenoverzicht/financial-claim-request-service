// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/financial-claim-request-service/pkg/model"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
)

type ConfigurationRepository interface {
	Create(ctx context.Context, newConfiguration model.Configuration) (*model.Configuration, error)
	List(ctx context.Context) (*[]model.Configuration, error)
	Get(ctx context.Context, id string) (*model.Configuration, error)
	Update(ctx context.Context, newConfiguration model.Configuration, id string) error
	Delete(ctx context.Context, id string) error
	healthcheck.Checker
}
