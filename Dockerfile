FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/financial-claim-request-service/api
COPY ./cmd /go/src/financial-claim-request-service/cmd
COPY ./internal /go/src/financial-claim-request-service/internal
COPY ./pkg /go/src/financial-claim-request-service/pkg
COPY ./go.mod /go/src/financial-claim-request-service/
COPY ./go.sum /go/src/financial-claim-request-service/
WORKDIR /go/src/financial-claim-request-service
RUN go mod download \
 && go build -o dist/bin/financial-claim-request-service ./cmd/financial-claim-request-service

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/financial-claim-request-service/dist/bin/financial-claim-request-service /usr/local/bin/financial-claim-request-service
COPY --from=build /go/src/financial-claim-request-service/api/openapi.json /api/openapi.json
COPY --from=build /go/src/financial-claim-request-service/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/financial-claim-request-service"]
